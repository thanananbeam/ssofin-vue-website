import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueRouter from 'vue-router'


// component
import App from './App.vue'

Vue.use(VueRouter)
Vue.use(VueI18n)

import routes from './routes.js'
import i18n from './i18n.js'
import store from './store/store.js'
let _dirPathName = process.env.BASE_URL
// console.log(process.env)
//// css
//require('../wwwroot/css/style.css')

//// other js plugins
//require('jquery')
//require('bootstrap/dist/js/bootstrap.bundle.min.js')

new Vue({
    el: '#app',
    router: new VueRouter({
        mode: 'history',
        routes: routes,
        base: _dirPathName,
        scrollBehavior(to, from, savedPosition) {
            if (savedPosition) {
                return savedPosition
            } else {
                return { x: 0, y: 0 }
            }
        }
    }),
    i18n,
    store,
    render: h => h(App)
})
