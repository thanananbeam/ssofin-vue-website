# sofinwebsite



## Install
```
npm install -g @vue/cli
```
npm install -g @vue/cli-service-global
```
vue create sofinwebsite
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## if deploy on production example vue.config_prod_example.js

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/donain-name/'
      : '/'
}

## if deploy on localhost example vue.config_prod_example.js

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/'
      : '/'
}
```